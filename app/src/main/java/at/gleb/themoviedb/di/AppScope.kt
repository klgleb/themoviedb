package at.gleb.themoviedb.di

import javax.inject.Scope

@Scope
@Retention
annotation class App