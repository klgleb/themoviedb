package at.gleb.themoviedb.di.modules

import at.gleb.themoviedb.data.jackson.MediaTypeDeserializer
import at.gleb.themoviedb.di.App
import at.gleb.themoviedb.domain.entities.media.Media
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import dagger.Module
import dagger.Provides
import javax.inject.Named

const val JACKSON_MAPPER = "JACKSON_MAPPER"

@Module
class JacksonMapperModule {

	@App
	@Provides
	fun provideMediaTypeDeserializer() = MediaTypeDeserializer()

	@App
	@Provides
	@Named(JACKSON_MAPPER)
	fun mapperKotlin(deserializer: MediaTypeDeserializer): ObjectMapper = jacksonObjectMapper()
			.registerKotlinModule()
			.registerModule(SimpleModule().apply {
				addDeserializer(Media::class.java, deserializer)
			})
			.setSerializationInclusion(JsonInclude.Include.NON_NULL);
}