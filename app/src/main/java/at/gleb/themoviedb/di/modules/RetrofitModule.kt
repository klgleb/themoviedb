package at.gleb.themoviedb.di.modules

import at.gleb.themoviedb.BuildConfig
import at.gleb.themoviedb.di.App
import com.fasterxml.jackson.databind.ObjectMapper
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import javax.inject.Named


const val RETROFIT_BUILDER = "RETROFIT_BUILDER"

@Module
class RetrofitModule {

	@App
	@Provides
	@Named(RETROFIT_BUILDER)
	fun retrofitBuilder(
			@Named(OK_HTTP_CLIENT) client: OkHttpClient,
			@Named(JACKSON_MAPPER) mapper: ObjectMapper
	): Retrofit.Builder = Retrofit.Builder()
			.client(client)
			.addConverterFactory(JacksonConverterFactory.create(mapper))
			.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
			.baseUrl(BuildConfig.SERVER_URL)
}
