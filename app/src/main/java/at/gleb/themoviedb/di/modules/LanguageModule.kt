package at.gleb.themoviedb.di.modules

import at.gleb.themoviedb.data.system.language.AndroidLanguageProvider
import at.gleb.themoviedb.data.system.language.LanguageProvider
import at.gleb.themoviedb.di.App
import dagger.Module
import dagger.Provides

@Module
class LanguageModule {
	@App
	@Provides
	fun languageProvider(): LanguageProvider = AndroidLanguageProvider()
}