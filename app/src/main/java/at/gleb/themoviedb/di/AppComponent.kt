package at.gleb.themoviedb.di

import android.app.Application
import android.content.SharedPreferences
import at.gleb.themoviedb.data.rest.di.MoviesApiModule
import at.gleb.themoviedb.data.system.preferences.Prefs
import at.gleb.themoviedb.data.system.preferences.di.PreferencesModule
import at.gleb.themoviedb.di.modules.JacksonMapperModule
import at.gleb.themoviedb.di.modules.LanguageModule
import at.gleb.themoviedb.di.modules.OkHttpClientModule
import at.gleb.themoviedb.di.modules.RetrofitModule
import at.gleb.themoviedb.domain.interactors.ImageConfigurationInteractor
import at.gleb.themoviedb.domain.interactors.PersonInteractor
import at.gleb.themoviedb.domain.interactors.PopularMoviesInteractor
import at.gleb.themoviedb.presentation.popular.di.PopularMoviesModule
import dagger.BindsInstance
import dagger.Component

@App
@Component(
		modules = [
			JacksonMapperModule::class,
			OkHttpClientModule::class,
			PreferencesModule::class,
			RetrofitModule::class,
			MoviesApiModule::class,
			PopularMoviesModule::class,
			LanguageModule::class
		]
)
interface AppComponent {
	val prefs: Prefs
	val sharedPreferences: SharedPreferences
	val moviesInteractor: PopularMoviesInteractor
	val personInteractor: PersonInteractor
	val imageConfigInteractor: ImageConfigurationInteractor
	val context: Application

	@Component.Builder
	interface Builder {

		fun build(): AppComponent

		@BindsInstance
		fun application(application: Application): Builder

	}
}