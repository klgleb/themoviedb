package at.gleb.themoviedb.di.modules

import at.gleb.themoviedb.di.App
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import javax.inject.Named


const val OK_HTTP_CLIENT = "OK_HTTP_CLIENT"
const val API_KEY_INTERCEPTOR = "API_KEY_INTERCEPTOR"

@Module
class OkHttpClientModule {
	@App
	@Provides
	fun okHttpClientBuilder() = OkHttpClient.Builder()

	@App
	@Provides
	@Named(OK_HTTP_CLIENT)
	fun okHttpClient(okHttClientBuilder: OkHttpClient.Builder, @Named(API_KEY_INTERCEPTOR) apiKeyInterceptor: Interceptor): OkHttpClient =
			okHttClientBuilder
					.addInterceptor(apiKeyInterceptor)
					.build()


	@App
	@Provides
	@Named(API_KEY_INTERCEPTOR)
	fun addApiKeyInterceptor() = Interceptor { chain ->
		var request = chain.request()
		val url = request.url().newBuilder().addQueryParameter("api_key", "b9d384470f66cd88cbaa66bcc4b965f6").build()
		request = request.newBuilder().url(url).build()
		chain.proceed(request)
	}
}