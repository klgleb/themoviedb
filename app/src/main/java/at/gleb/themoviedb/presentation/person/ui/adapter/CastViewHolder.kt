package at.gleb.themoviedb.presentation.person.ui.adapter

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import at.gleb.themoviedb.R
import at.gleb.themoviedb.data.glide.GlideApp
import at.gleb.themoviedb.data.glide.posterImageUrl
import at.gleb.themoviedb.domain.entities.CastEntity
import at.gleb.themoviedb.domain.entities.ImagesConfiguration
import at.gleb.themoviedb.util.px


class CastViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
	private val titleTextView: TextView = itemView.findViewById(R.id.titleTextView)
	private val imageView: ImageView = itemView.findViewById(R.id.imageView)

	fun bindData(entity: CastEntity?, imagesConfiguration: ImagesConfiguration, clickListener: () -> Unit) {
		imageView.setImageResource(android.R.color.transparent)
		titleTextView.text = entity?.title ?: ""
		itemView.setOnClickListener { clickListener.invoke() }

		GlideApp.with(imageView)
				.load(entity?.imagePath?.posterImageUrl(48.px, imagesConfiguration))
				.into(imageView)
	}
}