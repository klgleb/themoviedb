package at.gleb.themoviedb.presentation.person.presenter

import at.gleb.themoviedb.App
import at.gleb.themoviedb.domain.entities.ImagesConfiguration
import at.gleb.themoviedb.domain.entities.PersonDetailsResponse
import at.gleb.themoviedb.presentation.person.view.PersonView
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.BiFunction

@InjectViewState
class PersonPresenter(private val personId: Int) : MvpPresenter<PersonView>() {
	private lateinit var disposable: CompositeDisposable
	private val personInteractor = App.dagger.personInteractor
	private val imagesConfigurationInteractor = App.dagger.imageConfigInteractor

	override fun onFirstViewAttach() {
		super.onFirstViewAttach()

		if (personId == 0) {
			viewState.showErrorCantLoadPerson()
			return
		} else {
			disposable = CompositeDisposable(

					personInteractor.loadPersonDetails(personId)
							.zipWith(imagesConfigurationInteractor.getConfig(),
							         BiFunction { t1: PersonDetailsResponse, t2: ImagesConfiguration -> t1 to t2 })
							.subscribe(
									{
										val (response, config) = it
										viewState.setBiography(response.biography ?: "")
										response.profilePath?.let { path ->
											viewState.setImagePath(path, config)
										}
									}, {
										viewState.showUnknownError()
										it.printStackTrace()
									}
							)
			)
		}


	}

	override fun detachView(view: PersonView?) {
		super.attachView(view)
	}

	override fun onDestroy() {
		super.onDestroy()
		disposable.dispose()
	}
}
