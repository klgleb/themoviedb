package at.gleb.themoviedb.presentation.person.view

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType


interface InfoView : MvpView {
	@StateStrategyType(SingleStateStrategy::class)
	fun setBiography(biography: String)
}