package at.gleb.themoviedb.presentation.popular.view.ui

/*
 * Copyright 2015 Uwe Trottmann
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

/**
 * Adds spacing in between items in a grid. Meaning, there will be no margin added at the outer
 * edges of the grid.
 */
class GridInsetDecoration(private val insetHorizontal: Int, private val insetVertical: Int) : RecyclerView.ItemDecoration() {
	override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView,
	                            state: RecyclerView.State
	) {
		val layoutParams = view.layoutParams as GridLayoutManager.LayoutParams

		val position = layoutParams.viewPosition
		if (position == RecyclerView.NO_POSITION) {
			outRect.set(0, 0, 0, 0)
			return
		}

		// add edge margin only if item edge is not the grid edge
		val itemSpanIndex = layoutParams.spanIndex
		// is left grid edge?
		outRect.left = if (itemSpanIndex == 0) 0 else insetHorizontal
		// is top grid edge?
		outRect.top = if (itemSpanIndex == position) 0 else insetVertical
		outRect.right = 0
		outRect.bottom = 0
	}
}