package at.gleb.themoviedb.presentation.popular.view.ui.adapter.popular

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import at.gleb.themoviedb.R
import at.gleb.themoviedb.data.glide.GlideApp
import at.gleb.themoviedb.domain.entities.ImagesConfiguration
import at.gleb.themoviedb.domain.entities.media.MediaType


class PopularViewHolder(itemView: View, private val width: Int) : RecyclerView.ViewHolder(itemView) {
	private val titleTextView: TextView = itemView.findViewById(R.id.titleTextView)
	private val imageView: ImageView = itemView.findViewById(R.id.imageView)

	fun bindData(movieEntity: MediaType.Movie?, config: ImagesConfiguration, clickListener: () -> Unit) {
		imageView.setImageResource(android.R.color.transparent)
		titleTextView.text = movieEntity?.title ?: ""
		imageView.setOnClickListener { clickListener.invoke() }

		GlideApp.with(imageView).load(movieEntity?.img(width, config)).into(imageView)
	}
}