package at.gleb.themoviedb.presentation.popular.view.ui.adapter.search

import androidx.recyclerview.widget.DiffUtil
import at.gleb.themoviedb.domain.entities.SearchEntity

class SearchDiffCallback : DiffUtil.ItemCallback<SearchEntity>() {
	override fun areItemsTheSame(oldItem: SearchEntity, newItem: SearchEntity): Boolean {
		return oldItem.type.num == newItem.type.num && oldItem.entityId == newItem.entityId
	}

	override fun areContentsTheSame(oldItem: SearchEntity, newItem: SearchEntity): Boolean {
		return oldItem.title == newItem.title && oldItem.imagePath == newItem.imagePath
	}
}
