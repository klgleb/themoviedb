package at.gleb.themoviedb.presentation.person.ui.main

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import at.gleb.themoviedb.R
import at.gleb.themoviedb.data.CastType
import at.gleb.themoviedb.presentation.person.ui.fragment.CastFragment
import at.gleb.themoviedb.presentation.person.ui.fragment.InfoTabFragment

private val TAB_TITLES = arrayOf(
		R.string.tab_text_1,
		R.string.tab_text_2,
		R.string.tab_text_3
)

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
class SectionsPagerAdapter(private val context: Context, fm: FragmentManager, private val biography: String, private val personId: Int) :
		FragmentPagerAdapter(fm) {


	override fun getItem(position: Int): Fragment {
		return when (position) {
			0 -> InfoTabFragment.newInstance(biography)
			1 -> CastFragment.newInstance(personId, CastType.MOVIES)
			2 -> CastFragment.newInstance(personId, CastType.TV)
			else -> throw IndexOutOfBoundsException("Calling getItem($position), but tab count is $count")
		}
	}

	override fun getPageTitle(position: Int): CharSequence? {
		return context.resources.getString(TAB_TITLES[position])
	}

	override fun getCount(): Int {
		return 3
	}
}