package at.gleb.themoviedb.presentation.popular

import android.content.Intent
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.View
import android.widget.Toast
import androidx.paging.PagedList
import androidx.recyclerview.widget.GridLayoutManager
import at.gleb.themoviedb.Extras
import at.gleb.themoviedb.R
import at.gleb.themoviedb.data.glide.ImageQuality
import at.gleb.themoviedb.data.glide.posterImageUrl
import at.gleb.themoviedb.domain.entities.ImagesConfiguration
import at.gleb.themoviedb.domain.entities.SearchEntity
import at.gleb.themoviedb.domain.entities.media.MediaType
import at.gleb.themoviedb.presentation.moviedetails.MovieDetailsActivity
import at.gleb.themoviedb.presentation.person.PersonActivity
import at.gleb.themoviedb.presentation.popular.presenter.PopularPresenter
import at.gleb.themoviedb.presentation.popular.view.PopularView
import at.gleb.themoviedb.presentation.popular.view.ui.GridInsetDecoration
import at.gleb.themoviedb.presentation.popular.view.ui.adapter.popular.PopularRecyclerViewAdapter
import at.gleb.themoviedb.presentation.popular.view.ui.adapter.search.SearchRecyclerViewAdapter
import at.gleb.themoviedb.presentation.popular.view.ui.adapter.search.WrapContentLinearLayoutManager
import at.gleb.themoviedb.util.px
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.jakewharton.rxbinding3.appcompat.queryTextChangeEvents
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_main.*
import java.util.concurrent.TimeUnit

class PopularMoviesActivity : MvpAppCompatActivity(), PopularView {


	@InjectPresenter
	lateinit var presenter: PopularPresenter

	@ProvidePresenter
	fun providePresenter() = PopularPresenter()

	private lateinit var adapter: PopularRecyclerViewAdapter
	private lateinit var searchAdapter: SearchRecyclerViewAdapter

	private lateinit var disposable: CompositeDisposable

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)


		val displayMetrics = DisplayMetrics()
		windowManager.defaultDisplay.getMetrics(displayMetrics)
		val width = displayMetrics.widthPixels

		adapter = PopularRecyclerViewAdapter(width / 3) {
			presenter.onMovieClick(it)
		}

		recyclerView.adapter = adapter
		recyclerView.layoutManager = GridLayoutManager(this, 2)

		recyclerView.addItemDecoration(GridInsetDecoration(4.px, 4.px))

		searchAdapter = SearchRecyclerViewAdapter(presenter::onSearchClick)
		searchRecyclerView.layoutManager = WrapContentLinearLayoutManager(this)
		searchRecyclerView.adapter = searchAdapter

		disposable = CompositeDisposable(
				searchView.queryTextChangeEvents()
						.doOnNext {
							if (it.queryText.isBlank()) {
								searchRecyclerView.visibility = View.GONE
							}
						}
						.debounce(100, TimeUnit.MILLISECONDS)
						.subscribe(
								{
									presenter.onSearch(it.queryText)
								}, {
									it.printStackTrace()
								}
						)
		)

		searchView.setOnCloseListener {
			searchRecyclerView.visibility = View.GONE
			return@setOnCloseListener true
		}
	}

	override fun onDestroy() {
		super.onDestroy()
		disposable.dispose()
	}

	override fun submitMovies(data: PagedList<MediaType.Movie>, configuration: ImagesConfiguration) {
		adapter.imagesConfiguration = configuration
		adapter.submitList(data)
	}

	override fun submitSearchResults(data: List<SearchEntity>, imagesConfiguration: ImagesConfiguration) {
		searchAdapter.imagesConfiguration = imagesConfiguration
		searchAdapter.submitList(data)

		if (data.isNotEmpty()) {
			searchRecyclerView.visibility = View.VISIBLE
		} else {
			searchRecyclerView.visibility = View.GONE
		}
	}

	override fun navigateToMovieDetails(imagePath: String, description: String, title: String, imagesConfiguration: ImagesConfiguration) {
		val intent = Intent(this, MovieDetailsActivity::class.java).apply {
			putExtra(Extras.IMAGE, imagePath.posterImageUrl(ImageQuality.MAX, imagesConfiguration))
			putExtra(Extras.OVERVIEW, description)
			putExtra(Extras.TITLE, title)
		}
		startActivity(intent)
	}

	override fun navigateToPerson(personId: Int) {
		val intent = Intent(this, PersonActivity::class.java).apply {
			putExtra(Extras.PERSON_ID, personId)
		}
		startActivity(intent)
	}

	override fun showError(s: String) {
		Toast.makeText(this, s, Toast.LENGTH_LONG).show()
	}

	override fun showProgress() {
		//		TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
	}

	override fun hideProgress() {
		//		TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
	}
}
