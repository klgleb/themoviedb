package at.gleb.themoviedb.presentation.person

import android.os.Bundle
import android.util.DisplayMetrics
import android.widget.Toast
import at.gleb.themoviedb.Extras
import at.gleb.themoviedb.R
import at.gleb.themoviedb.data.glide.GlideApp
import at.gleb.themoviedb.data.glide.profileImageUrl
import at.gleb.themoviedb.domain.entities.ImagesConfiguration
import at.gleb.themoviedb.presentation.person.presenter.PersonPresenter
import at.gleb.themoviedb.presentation.person.ui.main.SectionsPagerAdapter
import at.gleb.themoviedb.presentation.person.view.PersonView
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.activity_person.*

class PersonActivity : MvpAppCompatActivity(), PersonView {
	@InjectPresenter
	lateinit var presenter: PersonPresenter

	private val personId
		get() = intent.getIntExtra(Extras.PERSON_ID, 0)

	@ProvidePresenter
	fun providePresenter() = PersonPresenter(personId)


	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_person)

		val displayMetrics = DisplayMetrics()
		windowManager.defaultDisplay.getMetrics(displayMetrics)
		val height = displayMetrics.heightPixels
		imageView.layoutParams.height = height / 2

		viewPager.offscreenPageLimit = 3
	}

	override fun setBiography(biography: String) {
		val sectionsPagerAdapter = SectionsPagerAdapter(this, supportFragmentManager, biography, personId)
		viewPager.adapter = sectionsPagerAdapter
		tabs.setupWithViewPager(viewPager)
	}

	override fun setImagePath(imagePath: String, config: ImagesConfiguration) {
		GlideApp.with(this).load(imagePath.profileImageUrl(imageView.width, config)).into(imageView)
	}

	override fun showErrorCantLoadPerson() {
		Toast.makeText(this, R.string.error_cant_load_person, Toast.LENGTH_SHORT).show()
	}

	override fun showUnknownError() {
		Toast.makeText(this, R.string.error_unknown, Toast.LENGTH_SHORT).show()
	}

	override fun showProgress() {
		TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
	}

	override fun hideProgress() {
		TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
	}

}