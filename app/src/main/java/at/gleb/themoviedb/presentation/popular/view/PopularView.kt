package at.gleb.themoviedb.presentation.popular.view

import androidx.paging.PagedList
import at.gleb.themoviedb.domain.entities.ImagesConfiguration
import at.gleb.themoviedb.domain.entities.SearchEntity
import at.gleb.themoviedb.domain.entities.media.MediaType
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

@StateStrategyType(AddToEndSingleStrategy::class)
interface PopularView : MvpView {
	fun submitMovies(data: PagedList<MediaType.Movie>, configuration: ImagesConfiguration)
	fun submitSearchResults(data: List<SearchEntity>, imagesConfiguration: ImagesConfiguration)
	fun showError(s: String)

	@StateStrategyType(SkipStrategy::class)
	fun navigateToMovieDetails(imagePath: String, description: String, title: String, imagesConfiguration: ImagesConfiguration)

	@StateStrategyType(SkipStrategy::class)
	fun navigateToPerson(personId: Int)

	fun showProgress()
	fun hideProgress()
}