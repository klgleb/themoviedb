package at.gleb.themoviedb.presentation.moviedetails

import android.os.Bundle
import android.util.DisplayMetrics
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import at.gleb.themoviedb.Extras
import at.gleb.themoviedb.R
import at.gleb.themoviedb.data.glide.GlideApp
import kotlinx.android.synthetic.main.activity_movie_details.*
import kotlinx.android.synthetic.main.content_movie_details.*


class MovieDetailsActivity : AppCompatActivity() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_movie_details)
		setSupportActionBar(toolbar)

		supportActionBar?.setDisplayHomeAsUpEnabled(true)

		val displayMetrics = DisplayMetrics()
		windowManager.defaultDisplay.getMetrics(displayMetrics)
		val height = displayMetrics.heightPixels


		appBar.layoutParams.height = height / 2


		intent.getStringExtra(Extras.IMAGE)?.let {
			GlideApp.with(this).load(it).into(posterImageView)
		}

		(intent.getStringExtra(Extras.TITLE) ?: "").let {
			supportActionBar?.title = it
		}

		(intent.getStringExtra(Extras.OVERVIEW) ?: "").let {
			overviewText.text = it
		}

	}

	override fun onOptionsItemSelected(item: MenuItem?): Boolean {
		return when (item?.itemId) {
			android.R.id.home -> {
				finish()
				true
			}

			else -> super.onOptionsItemSelected(item)
		}
	}
}
