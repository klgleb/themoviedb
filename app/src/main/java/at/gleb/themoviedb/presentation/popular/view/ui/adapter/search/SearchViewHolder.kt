package at.gleb.themoviedb.presentation.popular.view.ui.adapter.search

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import at.gleb.themoviedb.data.glide.GlideApp
import at.gleb.themoviedb.domain.entities.ImagesConfiguration
import at.gleb.themoviedb.domain.entities.SearchEntity
import at.gleb.themoviedb.util.px
import kotlinx.android.synthetic.main.search_item.view.*

class SearchViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
	fun bindData(entity: SearchEntity?, viewType: Int, imagesConfiguration: ImagesConfiguration, clickListener: () -> Unit) {
		itemView.setOnClickListener { clickListener.invoke() }

		if (viewType == TYPE_MAIN) {
			itemView.imageView.setImageResource(android.R.color.transparent)
			itemView.titleTextView.text = entity?.title
			GlideApp.with(itemView.imageView)
					.load(entity?.data?.img(48.px, imagesConfiguration))
					.into(itemView.imageView)
		}
	}
}