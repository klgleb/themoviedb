package at.gleb.themoviedb.presentation.person.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import at.gleb.themoviedb.R
import at.gleb.themoviedb.presentation.person.presenter.InfoPresenter
import at.gleb.themoviedb.presentation.person.view.InfoView
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.fragment_info.view.*

class InfoTabFragment : MvpAppCompatFragment(), InfoView {
	@InjectPresenter
	lateinit var presenter: InfoPresenter

	@ProvidePresenter
	fun providePresenter() = InfoPresenter(arguments?.getString(ARG_BIOGRAPHY)!!)

	lateinit var textView: TextView

	override fun onCreateView(
			inflater: LayoutInflater, container: ViewGroup?,
			savedInstanceState: Bundle?
	): View? {
		val root = inflater.inflate(R.layout.fragment_info, container, false)
		textView = root.textView
		return root
	}

	companion object {
		private const val ARG_BIOGRAPHY = "biography"

		/**
		 * Returns a new instance of this fragment for the given section
		 * number.
		 */
		@JvmStatic
		fun newInstance(biography: String): InfoTabFragment {
			return InfoTabFragment().apply {
				arguments = Bundle().apply {
					putString(ARG_BIOGRAPHY, biography)
				}
			}
		}
	}

	override fun setBiography(biography: String) {
		textView.text = biography
	}
}