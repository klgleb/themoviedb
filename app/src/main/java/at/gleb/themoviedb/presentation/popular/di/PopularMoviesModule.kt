package at.gleb.themoviedb.presentation.popular.di

import androidx.paging.Config
import androidx.paging.PagedList
import at.gleb.themoviedb.data.rest.TemoviedbApiService
import at.gleb.themoviedb.data.system.language.LanguageProvider
import at.gleb.themoviedb.data.system.preferences.Prefs
import at.gleb.themoviedb.di.App
import at.gleb.themoviedb.domain.interactors.PopularMoviesInteractor
import dagger.Module
import dagger.Provides

@Module
class PopularMoviesModule {
	@App
	@Provides
	fun providePagingConfig() = Config(10, enablePlaceholders = false)

	@App
	@Provides
	fun provideMoviesInteractor(currencyApiService: TemoviedbApiService,
	                            prefs: Prefs,
	                            languageProvider: LanguageProvider,
	                            pagingConfig: PagedList.Config
	) = PopularMoviesInteractor(
			currencyApiService,
			prefs,
			languageProvider,
			pagingConfig)
}