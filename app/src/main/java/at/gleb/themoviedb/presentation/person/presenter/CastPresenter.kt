package at.gleb.themoviedb.presentation.person.presenter

import at.gleb.themoviedb.App
import at.gleb.themoviedb.data.CastType
import at.gleb.themoviedb.domain.entities.CastEntity
import at.gleb.themoviedb.domain.entities.ImagesConfiguration
import at.gleb.themoviedb.domain.entities.media.Media
import at.gleb.themoviedb.domain.entities.media.MediaType
import at.gleb.themoviedb.presentation.person.view.CastView
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.BiFunction

@InjectViewState
class CastPresenter(private val personId: Int?, private val castType: CastType?) : MvpPresenter<CastView>() {

	private val interactor = App.dagger.personInteractor
	private val imageConfig = App.dagger.imageConfigInteractor
	private lateinit var disposable: CompositeDisposable


	override fun onFirstViewAttach() {
		super.onFirstViewAttach()

		if (personId == null || castType == null) {
			//			viewState.showError()
			return
		}

		disposable = CompositeDisposable(
				Single.just(castType)
						.flatMap {
							when (castType) {
								CastType.TV -> interactor.loadPersonTv(personId)
								CastType.MOVIES -> interactor.loadPersonMovies(personId)
							}
						}
						.zipWith(imageConfig.getConfig(), BiFunction { t1: List<Media>, t2: ImagesConfiguration -> t1 to t2 })
						.subscribe(
								{
									val (list, config) = it
									viewState.showCasts(list.mapNotNull { media ->
										when (media) {
											is MediaType.Movie -> CastEntity(
													id = media.id,
													imagePath = media.posterPath,
													title = media.title ?: "",
													type = CastType.MOVIES,
													data = media
											)
											is MediaType.Tv -> CastEntity(
													id = media.id,
													imagePath = media.posterPath,
													title = media.name ?: "",
													type = CastType.TV,
													data = media
											)
											else -> null
										}
									}, config)
								}, {
									it.printStackTrace()
								}
						)
		)
	}

	override fun onDestroy() {
		super.onDestroy()
		disposable.dispose()
	}

	fun onCastClick(castEntity: CastEntity) {
		val data = castEntity.data ?: return

		disposable.add(
				imageConfig.getConfig().subscribe(
						{
							when (data) {
								is MediaType.Tv -> viewState.navigateToMovieDetails(data.posterPath ?: "",
								                                                    data.overview ?: "",
								                                                    data.name ?: "",
								                                                    it)
								is MediaType.Movie -> viewState.navigateToMovieDetails(data.posterPath ?: "",
								                                                       data.overview ?: "",
								                                                       data.title ?: "",
								                                                       it)
							}
						}, {
							it.printStackTrace()
						}
				)
		)
	}
}
