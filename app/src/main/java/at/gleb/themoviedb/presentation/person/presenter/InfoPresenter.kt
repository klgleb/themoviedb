package at.gleb.themoviedb.presentation.person.presenter

import at.gleb.themoviedb.presentation.person.view.InfoView
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter

@InjectViewState
class InfoPresenter(private val biography: String) : MvpPresenter<InfoView>() {
	override fun onFirstViewAttach() {
		super.onFirstViewAttach()
		viewState.setBiography(biography)
	}
}
