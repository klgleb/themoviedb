package at.gleb.themoviedb.presentation.person.ui.adapter

import androidx.recyclerview.widget.DiffUtil
import at.gleb.themoviedb.domain.entities.CastEntity

class CastViewDiffCallback : DiffUtil.ItemCallback<CastEntity>() {
	override fun areItemsTheSame(oldItem: CastEntity, newItem: CastEntity): Boolean {
		return oldItem.id == newItem.id && oldItem.type == newItem.type
	}

	override fun areContentsTheSame(oldItem: CastEntity, newItem: CastEntity): Boolean {
		return oldItem.title == newItem.title && oldItem.imagePath == newItem.imagePath
	}
}
