package at.gleb.themoviedb.presentation.person.ui.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import at.gleb.themoviedb.Extras
import at.gleb.themoviedb.R
import at.gleb.themoviedb.data.CastType
import at.gleb.themoviedb.data.glide.ImageQuality
import at.gleb.themoviedb.data.glide.posterImageUrl
import at.gleb.themoviedb.data.toCastType
import at.gleb.themoviedb.domain.entities.CastEntity
import at.gleb.themoviedb.domain.entities.ImagesConfiguration
import at.gleb.themoviedb.presentation.moviedetails.MovieDetailsActivity
import at.gleb.themoviedb.presentation.person.presenter.CastPresenter
import at.gleb.themoviedb.presentation.person.ui.adapter.CastRecyclerViewAdapter
import at.gleb.themoviedb.presentation.person.view.CastView
import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.fragment_cast.view.*

class CastFragment : MvpAppCompatFragment(), CastView {

	@InjectPresenter
	lateinit var presenter: CastPresenter

	private lateinit var adapter: CastRecyclerViewAdapter

	@ProvidePresenter
	fun providePresenter() = CastPresenter(
			arguments?.getInt(ARG_PERSON_ID, 0),
			arguments?.getInt(ARG_CAST_TYPE, 0).toCastType()
	)

	override fun onCreateView(
			inflater: LayoutInflater, container: ViewGroup?,
			savedInstanceState: Bundle?
	): View? {
		val view = inflater.inflate(R.layout.fragment_cast, container, false)
		initRecyclerView(view.recyclerView)
		return view
	}

	override fun showCasts(list: List<CastEntity>,
	                       config: ImagesConfiguration
	) {
		adapter.config = config
		adapter.submitList(list)
	}

	override fun showProgress() {
	}

	override fun hideProgress() {
	}

	override fun navigateToMovieDetails(imagePath: String, description: String, title: String, imagesConfiguration: ImagesConfiguration) {
		val intent = Intent(this.context, MovieDetailsActivity::class.java).apply {
			putExtra(Extras.IMAGE, imagePath.posterImageUrl(ImageQuality.MAX, imagesConfiguration))
			putExtra(Extras.OVERVIEW, description)
			putExtra(Extras.TITLE, title)
		}
		startActivity(intent)
	}

	private fun initRecyclerView(recyclerView: RecyclerView) {
		recyclerView.layoutManager = LinearLayoutManager(context)
		adapter = CastRecyclerViewAdapter {
			presenter.onCastClick(it)
		}
		recyclerView.adapter = adapter
	}

	companion object {
		private const val ARG_CAST_TYPE = "toCastType"
		private const val ARG_PERSON_ID = "personId"

		/**
		 * Returns a new instance of this fragment for the given section
		 * number.
		 */
		@JvmStatic
		fun newInstance(personId: Int, castType: CastType): CastFragment {
			return CastFragment().apply {
				arguments = Bundle().apply {
					putInt(ARG_PERSON_ID, personId)
					putInt(ARG_CAST_TYPE, castType.num)
				}
			}
		}
	}
}