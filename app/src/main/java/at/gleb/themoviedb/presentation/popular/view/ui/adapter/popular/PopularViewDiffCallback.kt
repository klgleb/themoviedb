package at.gleb.themoviedb.presentation.popular.view.ui.adapter.popular

import androidx.recyclerview.widget.DiffUtil
import at.gleb.themoviedb.domain.entities.media.MediaType

class PopularViewDiffCallback : DiffUtil.ItemCallback<MediaType.Movie>() {
	override fun areItemsTheSame(oldItem: MediaType.Movie, newItem: MediaType.Movie): Boolean {
		return oldItem.id == newItem.id
	}

	override fun areContentsTheSame(oldItem: MediaType.Movie, newItem: MediaType.Movie): Boolean {
		return oldItem.title == newItem.title && oldItem.posterPath == newItem.posterPath
	}
}
