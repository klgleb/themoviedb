package at.gleb.themoviedb.presentation.person.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import at.gleb.themoviedb.R
import at.gleb.themoviedb.domain.entities.CastEntity
import at.gleb.themoviedb.domain.entities.ImagesConfiguration

class CastRecyclerViewAdapter(val callback: (CastEntity) -> Unit) : ListAdapter<CastEntity, CastViewHolder>(
		CastViewDiffCallback()) {

	lateinit var config: ImagesConfiguration

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CastViewHolder {
		val inflater = LayoutInflater.from(parent.context)
		val itemView = inflater.inflate(R.layout.cast_item, parent, false)
		return CastViewHolder(itemView)
	}

	override fun onBindViewHolder(holder: CastViewHolder, position: Int) {
		val entity = getItem(position)
		holder.bindData(entity, config) {
			if (entity != null) {
				callback(entity)
			}
		}
	}
}