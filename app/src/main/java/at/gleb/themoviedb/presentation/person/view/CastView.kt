package at.gleb.themoviedb.presentation.person.view

import at.gleb.themoviedb.domain.entities.CastEntity
import at.gleb.themoviedb.domain.entities.ImagesConfiguration
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType


@StateStrategyType(AddToEndSingleStrategy::class)
interface CastView : MvpView {
	fun showCasts(list: List<CastEntity>, config: ImagesConfiguration)
	fun showProgress()
	fun hideProgress()

	@StateStrategyType(SkipStrategy::class)
	fun navigateToMovieDetails(imagePath: String, description: String, title: String, imagesConfiguration: ImagesConfiguration)
}