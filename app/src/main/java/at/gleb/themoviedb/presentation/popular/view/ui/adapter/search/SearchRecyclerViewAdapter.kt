package at.gleb.themoviedb.presentation.popular.view.ui.adapter.search

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import at.gleb.themoviedb.R
import at.gleb.themoviedb.domain.entities.ImagesConfiguration
import at.gleb.themoviedb.domain.entities.SearchEntity
import timber.log.Timber

const val TYPE_MAIN = 1
const val TYPE_SHOW_MORE = 2
private const val MAX_COUNT = 5

class SearchRecyclerViewAdapter(val callback: (SearchEntity) -> Unit) : ListAdapter<SearchEntity, SearchViewHolder>(SearchDiffCallback()) {

	lateinit var imagesConfiguration: ImagesConfiguration
	private var collapsed = true

	private val hasMoreButton
		get() = collapsed && (super.getItemCount() >= MAX_COUNT)


	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchViewHolder {
		val inflater = LayoutInflater.from(parent.context)
		val itemView = when (viewType) {
			TYPE_MAIN -> inflater.inflate(R.layout.search_item, parent, false)
			else -> inflater.inflate(R.layout.search_item_view_more, parent, false)
		}
		return SearchViewHolder(itemView)
	}

	override fun onBindViewHolder(holder: SearchViewHolder, position: Int) {

		val itemViewType = getItemViewType(position)
		if (itemViewType != TYPE_SHOW_MORE) {
			val entity = getItem(position)
			holder.bindData(entity, itemViewType, imagesConfiguration) {
				callback(entity)
			}
		} else {
			holder.bindData(null, itemViewType, imagesConfiguration) {
				collapsed = false
				notifyItemRangeInserted(position, super.getItemCount() - position)
			}
		}
	}

	override fun getItemCount(): Int {
		return if (hasMoreButton) {
			Timber.d("max+1")
			MAX_COUNT + 1
		} else {
			Timber.d("${super.getItemCount()}")
			super.getItemCount()
		}

	}

	override fun submitList(list: List<SearchEntity>?) {
		Timber.d("submit list $list")
		collapsed = true
		super.submitList(list)
	}

	override fun getItemViewType(position: Int): Int {
		return if (hasMoreButton && position == MAX_COUNT) {
			TYPE_SHOW_MORE
		} else {
			TYPE_MAIN
		}
	}
}