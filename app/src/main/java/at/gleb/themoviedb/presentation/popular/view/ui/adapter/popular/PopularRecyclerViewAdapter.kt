package at.gleb.themoviedb.presentation.popular.view.ui.adapter.popular

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import at.gleb.themoviedb.R
import at.gleb.themoviedb.domain.entities.ImagesConfiguration
import at.gleb.themoviedb.domain.entities.media.MediaType

class PopularRecyclerViewAdapter(private val imageWidth: Int,
                                 val callback: (MediaType.Movie) -> Unit
) : PagedListAdapter<MediaType.Movie, PopularViewHolder>(
		PopularViewDiffCallback()) {

	lateinit var imagesConfiguration: ImagesConfiguration

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PopularViewHolder {
		val inflater = LayoutInflater.from(parent.context)
		val itemView = inflater.inflate(R.layout.movie_item, parent, false)
		return PopularViewHolder(itemView, imageWidth)
	}

	override fun onBindViewHolder(holder: PopularViewHolder, position: Int) {
		val entity = getItem(position)
		holder.bindData(entity, imagesConfiguration) {
			if (entity != null) {
				callback(entity)
			}
		}
	}
}