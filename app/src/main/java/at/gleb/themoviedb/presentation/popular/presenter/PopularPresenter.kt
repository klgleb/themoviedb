package at.gleb.themoviedb.presentation.popular.presenter

import at.gleb.themoviedb.App
import at.gleb.themoviedb.domain.entities.ImagesConfiguration
import at.gleb.themoviedb.domain.entities.SearchEntity
import at.gleb.themoviedb.domain.entities.SearchResultType
import at.gleb.themoviedb.domain.entities.media.Media
import at.gleb.themoviedb.domain.entities.media.MediaType
import at.gleb.themoviedb.domain.interactors.PopularMoviesInteractor
import at.gleb.themoviedb.presentation.popular.view.PopularView
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable

@InjectViewState
class PopularPresenter : MvpPresenter<PopularView>() {

	private val moviesInteractor: PopularMoviesInteractor = App.dagger.moviesInteractor
	private val configurationInteractor = App.dagger.imageConfigInteractor
	private lateinit var disposable: CompositeDisposable
	private lateinit var imageConfig: ImagesConfiguration

	override fun onFirstViewAttach() {
		super.onFirstViewAttach()

		disposable = CompositeDisposable()

		disposable.add(
				configurationInteractor.getConfig()
						.doOnSubscribe { viewState.showProgress() }
						.subscribe(
								{
									imageConfig = it
									setupObservers()
								}, {
									it.printStackTrace()
									viewState.hideProgress()
									viewState.showError(it.toString())
								}
						)
		)


	}

	private fun setupObservers() {
		disposable.addAll(
				moviesInteractor.moviesData
						.subscribe(
								{
									viewState.submitMovies(it, imageConfig)
								}, {
									viewState.showError(it.toString())
								}
						),

				moviesInteractor.searchData
						.observeOn(AndroidSchedulers.mainThread())
						.subscribe(
								{ list ->
									viewState.submitSearchResults(list.map { media ->
										SearchEntity(
												type = when (media) {
													is MediaType.Person -> SearchResultType.ACTOR
													is MediaType.Movie -> SearchResultType.MOVIE
													is MediaType.Tv -> SearchResultType.TV
													else -> SearchResultType.UNKNOWN
												},
												title = when (media) {
													is MediaType.Person -> media.name
													is MediaType.Movie -> media.title
													is MediaType.Tv -> media.name
													else -> ""
												},
												imagePath = when (media) {
													is MediaType.Person -> media.profilePath
													is MediaType.Movie -> media.posterPath
													is MediaType.Tv -> media.posterPath
													else -> ""
												},
												entityId = media.id,
												data = media
										)
									}, imageConfig)
								}, {
									viewState.showError(it.toString())
									it.printStackTrace()
								}
						)
		)
	}

	override fun detachView(view: PopularView?) {
		super.attachView(view)
	}

	override fun onDestroy() {
		super.onDestroy()
		disposable.dispose()
	}

	fun onMovieClick(movieEntity: MediaType.Movie) {
		viewState.navigateToMovieDetails(movieEntity.posterPath ?: "", movieEntity.overview ?: "", movieEntity.title ?: "", imageConfig)
	}

	fun onSearchClick(entity: SearchEntity) {

		when (val data = entity.data as Media) {
			is MediaType.Movie -> viewState.navigateToMovieDetails(entity.imagePath ?: "",
			                                                       data.overview ?: "",
			                                                       data.title ?: "",
			                                                       imageConfig)
			is MediaType.Tv -> viewState.navigateToMovieDetails(entity.imagePath ?: "", "", data.name ?: "", imageConfig)
			is MediaType.Person -> viewState.navigateToPerson(data.id)
		}
	}

	fun onSearch(queryText: CharSequence) {
		if (queryText.isNotBlank()) {
			disposable.add(
					moviesInteractor.searchQuery(queryText.toString())
			)
		}
	}
}