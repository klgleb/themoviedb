package at.gleb.themoviedb.presentation.person.view

import at.gleb.themoviedb.domain.entities.ImagesConfiguration
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

@StateStrategyType(value = AddToEndSingleStrategy::class)
interface PersonView : MvpView {
	fun showErrorCantLoadPerson()
	fun showUnknownError()
	fun showProgress()
	fun hideProgress()
	fun setImagePath(imagePath: String, config: ImagesConfiguration)
	fun setBiography(biography: String)
}