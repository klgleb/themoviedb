package at.gleb.themoviedb

import androidx.multidex.MultiDexApplication
import at.gleb.prefdeletages.PrefDelegates
import at.gleb.themoviedb.di.AppComponent
import at.gleb.themoviedb.di.DaggerAppComponent
import timber.log.Timber
import timber.log.Timber.DebugTree


class App : MultiDexApplication() {
	override fun onCreate() {
		super.onCreate()
		init()
	}

	private fun init() {
		dagger = DaggerAppComponent.builder().application(this).build()
		PrefDelegates.init(dagger.sharedPreferences)

		Timber.plant(DebugTree())
	}

	companion object {
		lateinit var dagger: AppComponent
	}
}