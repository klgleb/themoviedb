package at.gleb.themoviedb.domain.entities.media

import androidx.annotation.WorkerThread
import at.gleb.themoviedb.data.glide.posterImageUrl
import at.gleb.themoviedb.data.glide.profileImageUrl
import at.gleb.themoviedb.domain.entities.ImagesConfiguration
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

interface Media : Serializable {
	val id: Int

	@WorkerThread
	fun img(width: Int, configuration: ImagesConfiguration): String? = when (this) {
		is MediaType.Person -> profilePath?.profileImageUrl(width, configuration)
		is MediaType.Movie -> posterPath?.posterImageUrl(width, configuration)
		is MediaType.Tv -> posterPath?.posterImageUrl(width, configuration)
		else -> null
	}
}

object MediaType {
	@JsonIgnoreProperties(ignoreUnknown = true)
	data class Movie(
			override val id: Int,
			val title: String? = null,
			@JsonProperty("poster_path")
			var posterPath: String? = null,
			val overview: String? = null
	) : Media

	@JsonIgnoreProperties(ignoreUnknown = true)
	data class Tv(
			override val id: Int,
			@JsonProperty("name")
			val name: String? = null,
			@JsonProperty("poster_path")
			var posterPath: String? = null,
			var overview: String? = null

	) : Media

	@JsonIgnoreProperties(ignoreUnknown = true)
	data class Person(
			override val id: Int,
			@JsonProperty("name")
			val name: String? = null,
			@JsonProperty("profile_path")
			var profilePath: String? = null,
			@JsonProperty("known_for")
			var knownFor: List<Movie>
	) : Media

	@JsonIgnoreProperties(ignoreUnknown = true)
	data class Unknown(
			override val id: Int
	) : Media
}

























