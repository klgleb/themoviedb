package at.gleb.themoviedb.domain.entities

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class PersonDetailsResponse(
		val id: Int? = null,
		val name: String? = null,
		val biography: String? = null,

		@JsonProperty("profile_path")
		var profilePath: String? = null
)