package at.gleb.themoviedb.domain.entities

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class PersonCreditsResponse<T>(val cast: List<T>)


