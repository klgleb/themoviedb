package at.gleb.themoviedb.domain.interactors

import androidx.annotation.IntRange
import androidx.paging.PagedList
import androidx.paging.RxPagedListBuilder
import at.gleb.themoviedb.data.movies.PopularMoviesDataSourceFactory
import at.gleb.themoviedb.data.rest.TemoviedbApiService
import at.gleb.themoviedb.data.system.language.LanguageProvider
import at.gleb.themoviedb.data.system.preferences.Prefs
import at.gleb.themoviedb.domain.entities.MoviesResponse
import at.gleb.themoviedb.domain.entities.media.Media
import at.gleb.themoviedb.domain.entities.media.MediaType
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class PopularMoviesInteractor @Inject constructor(
		private val apiService: TemoviedbApiService,
		private val prefs: Prefs,
		private val languageProvider: LanguageProvider,
		pagingConfig: PagedList.Config
) {

	private val popularMoviesDataSourceFactory = PopularMoviesDataSourceFactory(this)

	val moviesData: Observable<PagedList<MediaType.Movie>> =
			RxPagedListBuilder(popularMoviesDataSourceFactory, pagingConfig).buildObservable()

	val searchData: PublishSubject<List<Media>> = PublishSubject.create()

	fun getMovies(page: Int): Single<MoviesResponse> = apiService.popularMovies(languageProvider.language, page)
			.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread())

	fun searchQuery(queryText: String, @IntRange(from = 1, to = 1000) page: Int = 1) =
			apiService.multiSearch(queryText, languageProvider.language, page = page)
					.subscribeOn(Schedulers.io())
					.subscribe(
							{
								searchData.onNext(it.results)
							}, {
								it.printStackTrace()
							}
					)
}