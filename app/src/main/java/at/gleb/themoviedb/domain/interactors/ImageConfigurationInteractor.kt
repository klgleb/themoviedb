package at.gleb.themoviedb.domain.interactors

import at.gleb.themoviedb.data.repositories.ConfigurationRepository
import at.gleb.themoviedb.domain.entities.ImagesConfiguration
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ImageConfigurationInteractor @Inject constructor(private val configRepository: ConfigurationRepository) {
	fun getConfig(): Single<ImagesConfiguration> = configRepository
			.getImagesConfiguration()
			.subscribeOn(Schedulers.io())
			.cache()
}