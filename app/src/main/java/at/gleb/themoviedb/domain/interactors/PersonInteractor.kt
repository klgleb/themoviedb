package at.gleb.themoviedb.domain.interactors

import at.gleb.themoviedb.data.CastType
import at.gleb.themoviedb.data.repositories.PersonRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class PersonInteractor @Inject constructor(private val personRepository: PersonRepository) {
	fun loadPersonMovies(personId: Int) = personRepository
			.loadPersonData(CastType.MOVIES, personId = personId)
			.map { it.cast }
			.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread())

	fun loadPersonTv(personId: Int) = personRepository
			.loadPersonData(CastType.TV, personId = personId)
			.map { it.cast }
			.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread())

	fun loadPersonDetails(personId: Int) = personRepository
			.loadPersonDetails(personId = personId)
			.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread())
}