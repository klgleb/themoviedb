package at.gleb.themoviedb.domain.entities

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

@JsonIgnoreProperties(ignoreUnknown = true)
data class ConfigurationResponse(
		val images: ImagesConfiguration
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class ImagesConfiguration(
		@JsonProperty("base_url")
		val baseUrl: String? = null,

		@JsonProperty("secure_base_url")
		val secureBaseUrl: String? = null,

		@JsonProperty("profile_sizes")
		val profileSizes: List<String> = arrayListOf(),

		@JsonProperty("poster_sizes")
		val posterSizes: List<String> = arrayListOf()
) : Serializable {

	private val cachePosterQualities = hashMapOf<Pair<Int?, Int?>, String>()
	private val cacheProfileQualities = hashMapOf<Pair<Int?, Int?>, String>()

	fun posterQuality(minWidth: Int? = null, minHeight: Int? = null): String {
		if ((minWidth != null || minHeight != null) && cachePosterQualities.containsKey(minWidth to minHeight)) {
			return cachePosterQualities[minWidth to minHeight]!!
		}

		val posterWidths = posterSizes.filterNumbers("w").sorted()
		val posterHeights = posterSizes.filterNumbers("h").sorted()

		if (posterWidths.isNotEmpty() && minWidth != null || posterHeights.isEmpty() && minWidth == null && minHeight != null) {
			val requiredWidth = minWidth ?: (minHeight!! / 2) //poster height is usually half the height

			posterWidths.forEach {
				if (it >= requiredWidth) {
					cachePosterQualities[minWidth to minHeight] = "w$it"
					return "w$it"
				}
			}
		} else if (posterHeights.isNotEmpty() && minHeight != null) {
			posterHeights.forEach {
				if (it >= minHeight) {
					cachePosterQualities[minWidth to minHeight] = "h$it"
					return "h$it"
				}
			}
		}


		val res = when {
			posterSizes.contains("original") -> "original"
			posterSizes.isNotEmpty() -> posterSizes[posterSizes.size - 1]
			else -> throw UnsupportedOperationException("posterSizes is empty")
		}

		cachePosterQualities[minWidth to minHeight] = res

		return res
	}


	fun profileQuality(minWidth: Int? = null, minHeight: Int? = null): String {
		if ((minWidth != null || minHeight != null) && cacheProfileQualities.containsKey(minWidth to minHeight)) {
			return cacheProfileQualities[minWidth to minHeight]!!
		}

		val posterWidths = posterSizes.filterNumbers("w").sorted()
		val posterHeights = posterSizes.filterNumbers("h").sorted()

		if (posterWidths.isNotEmpty() && minWidth != null) {
			posterWidths.forEach {
				if (it >= minWidth) {
					cacheProfileQualities[minWidth to minHeight] = "w$it"
					return "w$it"
				}
			}
		} else if (posterHeights.isNotEmpty() && minHeight != null) {
			posterHeights.forEach {
				if (it >= minHeight) {
					cacheProfileQualities[minWidth to minHeight] = "h$it"
					return "h$it"
				}
			}
		}


		val res = when {
			posterSizes.contains("original") -> "original"
			posterSizes.isNotEmpty() -> posterSizes[posterSizes.size - 1]
			else -> throw UnsupportedOperationException("posterSizes is empty")
		}

		cacheProfileQualities[minWidth to minHeight] = res

		return res
	}


	private fun List<String>.filterNumbers(startSymbol: String): List<Int> = this
			.filter { it.startsWith(startSymbol) }
			.mapNotNull {
				it.substring(1).toIntOrNull()
			}

}