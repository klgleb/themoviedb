package at.gleb.themoviedb.domain.entities

import at.gleb.themoviedb.domain.entities.media.Media
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class SearchResponse(
		val page: Int? = null,
		val results: List<Media>,
		@JsonProperty("total_pages")
		val totalPages: Int? = null
)