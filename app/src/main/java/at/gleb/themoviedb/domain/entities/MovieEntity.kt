package at.gleb.themoviedb.domain.entities

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
@Deprecated("Use MediaType.Movie instead",
            replaceWith = ReplaceWith("MediaType.Movie", "at.gleb.themoviedb.domain.entities.media.MediaType"))
data class MovieEntity(
		val id: Int? = null,
		val title: String? = null,
		@JsonProperty("poster_path")
		var posterPath: String? = null,
		val overview: String? = null
)