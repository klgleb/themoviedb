package at.gleb.themoviedb.domain.entities

import at.gleb.themoviedb.domain.entities.media.MediaType
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class MoviesResponse(
		val page: Int? = 1,
		val results: List<MediaType.Movie>? = listOf(),
		@JsonProperty("total_pages")
		val totalPages: Int? = null

)