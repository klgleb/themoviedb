package at.gleb.themoviedb.domain.entities

import at.gleb.themoviedb.data.CastType
import at.gleb.themoviedb.domain.entities.media.Media

data class CastEntity(
		val type: CastType,
		val id: Int? = null,
		var imagePath: String? = null,
		val title: String = "",
		val data: Media? = null
)