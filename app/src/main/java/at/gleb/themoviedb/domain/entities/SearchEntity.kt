package at.gleb.themoviedb.domain.entities

import at.gleb.themoviedb.domain.entities.media.Media

data class SearchEntity(val type: SearchResultType,
                        val imagePath: String? = null,
                        val title: String? = null,
                        val entityId: Int? = null,
                        val data: Media? = null
)

enum class SearchResultType(val num: Int) {
	ACTOR(0),
	MOVIE(1),
	UNKNOWN(2),
	TV(3)
}