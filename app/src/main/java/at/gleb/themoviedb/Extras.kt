package at.gleb.themoviedb

object Extras {
	const val TITLE = "title"
	const val IMAGE = "image"
	const val OVERVIEW = "overview"
	const val PERSON_ID = "personID"
}