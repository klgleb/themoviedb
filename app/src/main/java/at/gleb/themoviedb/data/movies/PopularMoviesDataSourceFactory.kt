package at.gleb.themoviedb.data.movies

import androidx.paging.DataSource
import at.gleb.themoviedb.domain.entities.media.MediaType
import at.gleb.themoviedb.domain.interactors.PopularMoviesInteractor
import io.reactivex.subjects.BehaviorSubject

class PopularMoviesDataSourceFactory(private val interactor: PopularMoviesInteractor) :
		DataSource.Factory<Int, MediaType.Movie>() {
	val moviesSubject: BehaviorSubject<PopularMoviesDataSource> = BehaviorSubject.create()

	override fun create(): DataSource<Int, MediaType.Movie> {
		val dataSource = PopularMoviesDataSource(interactor)
		moviesSubject.onNext(dataSource)
		return dataSource
	}
}
