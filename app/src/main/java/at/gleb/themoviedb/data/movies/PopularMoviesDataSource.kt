package at.gleb.themoviedb.data.movies

import android.annotation.SuppressLint
import androidx.paging.PageKeyedDataSource
import at.gleb.themoviedb.domain.entities.media.MediaType
import at.gleb.themoviedb.domain.interactors.PopularMoviesInteractor
import javax.inject.Inject

class PopularMoviesDataSource @Inject constructor(
		private val interactor: PopularMoviesInteractor
) : PageKeyedDataSource<Int, MediaType.Movie>() {

	@SuppressLint("CheckResult")
	override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, MediaType.Movie>) {
		interactor.getMovies(1).subscribe(
				{
					callback.onResult(it.results!!, 1, it.totalPages!!, null, 2)
				}, {
					it.printStackTrace()
				}
		)

	}

	@SuppressLint("CheckResult")
	override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, MediaType.Movie>) {
		interactor.getMovies(params.key).subscribe(
				{
					callback.onResult(it.results!!, params.key + 1)
				}, {
					it.printStackTrace()
				}
		)

	}

	override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, MediaType.Movie>) {
		//ignored
	}
}