package at.gleb.themoviedb.data.rest.di

import at.gleb.themoviedb.BuildConfig
import at.gleb.themoviedb.data.rest.TemoviedbApiService
import at.gleb.themoviedb.di.App
import at.gleb.themoviedb.di.modules.RETROFIT_BUILDER
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Named

@Module
open class MoviesApiModule {

	@Provides
	@App
	fun provideApiService(@Named(RETROFIT_BUILDER) retrofitBuilder: Retrofit.Builder): TemoviedbApiService = retrofitBuilder
			.baseUrl(BuildConfig.SERVER_URL)
			.build()
			.create(TemoviedbApiService::class.java)

}