package at.gleb.themoviedb.data.glide

import at.gleb.themoviedb.domain.entities.ImagesConfiguration


fun String.posterImageUrl(width: Int, config: ImagesConfiguration): String? {
	if (!checkPath()) return null

	val baseUrl = config.secureBaseUrl
	return "$baseUrl${config.posterQuality(width)}$this"
}

fun String.profileImageUrl(width: Int, config: ImagesConfiguration): String? {
	if (!checkPath()) return null

	val baseUrl = config.secureBaseUrl
	return "$baseUrl${config.profileQuality(width)}$this"
}

fun String.posterImageUrl(quality: ImageQuality, config: ImagesConfiguration): String? {
	if (!checkPath()) return null

	val baseUrl = config.secureBaseUrl
	val w = when (quality) {
		ImageQuality.MAX -> Integer.MAX_VALUE
	}

	return "$baseUrl${config.profileQuality(w)}$this"
}

private fun String.checkPath(): Boolean {
	return this.startsWith("/") && this.length > 2
}

enum class ImageQuality {
	MAX
}