package at.gleb.themoviedb.data.jackson

import at.gleb.themoviedb.domain.entities.media.Media
import at.gleb.themoviedb.domain.entities.media.MediaType
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.node.IntNode
import java.io.IOException

class MediaTypeDeserializer @JvmOverloads constructor(vc: Class<*>? = null) : StdDeserializer<Media>(vc) {

	@Throws(IOException::class, JsonProcessingException::class)
	override fun deserialize(jsonParser: JsonParser, ctxt: DeserializationContext): Media {
		val node = jsonParser.codec.readTree<JsonNode>(jsonParser)
		val mediaTypeNode = node.get("media_type")

		val id = (node.get("id") as IntNode).numberValue() as Int

		val mediaTypeParser = node.traverse()
		return when (mediaTypeNode.asText()) {
			"movie" -> {
				jsonParser.codec.readValue(mediaTypeParser, MediaType.Movie::class.java)
			}
			"tv" -> {
				jsonParser.codec.readValue(mediaTypeParser, MediaType.Tv::class.java)
			}
			"person" -> {
				jsonParser.codec.readValue(mediaTypeParser, MediaType.Person::class.java)
			}
			else -> {
				MediaType.Unknown(id)
			}
		}
	}
}