package at.gleb.themoviedb.data.repositories

import at.gleb.themoviedb.data.CastType
import at.gleb.themoviedb.data.rest.TemoviedbApiService
import at.gleb.themoviedb.data.system.language.LanguageProvider
import at.gleb.themoviedb.domain.entities.PersonCreditsResponse
import at.gleb.themoviedb.domain.entities.PersonDetailsResponse
import at.gleb.themoviedb.domain.entities.media.Media
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class PersonRepository @Inject constructor(private val apiService: TemoviedbApiService, private val languageProvider: LanguageProvider) {


	fun loadPersonData(loadType: CastType, personId: Int): Single<PersonCreditsResponse<out Media>> =
			Single.defer {
				return@defer when (loadType) {
					CastType.MOVIES -> apiService.personMovieCredits(personId, languageProvider.language)
					CastType.TV -> apiService.personTvCredits(personId, languageProvider.language)
				}
			}
					.subscribeOn(Schedulers.io())
					.observeOn(AndroidSchedulers.mainThread())

	fun loadPersonDetails(personId: Int): Single<PersonDetailsResponse> = Single.fromCallable { languageProvider.language }
			.flatMap {
				apiService.personDetails(personId, it)
			}
			.subscribeOn(Schedulers.io())
			.observeOn(AndroidSchedulers.mainThread())
}
