package at.gleb.themoviedb.data.system.preferences.di

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import at.gleb.themoviedb.data.system.preferences.Prefs
import at.gleb.themoviedb.di.App
import dagger.Module
import dagger.Provides

@Module
class PreferencesModule {

	@App
	@Provides
	fun preferences(): Prefs = Prefs()

	@App
	@Provides
	fun provideSharedPreferences(context: Application): SharedPreferences =
			context.getSharedPreferences("at.gleb.themoviedb", Context.MODE_PRIVATE)
}