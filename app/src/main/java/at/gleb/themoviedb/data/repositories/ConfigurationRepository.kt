package at.gleb.themoviedb.data.repositories

import at.gleb.themoviedb.data.rest.TemoviedbApiService
import at.gleb.themoviedb.data.system.preferences.Prefs
import at.gleb.themoviedb.domain.entities.ImagesConfiguration
import io.reactivex.Single
import javax.inject.Inject

class ConfigurationRepository @Inject constructor(private val apiService: TemoviedbApiService, private val prefs: Prefs) {

	fun getImagesConfiguration(): Single<ImagesConfiguration> = Single.fromCallable { prefs.imagesConfiguration }
			.flatMap { configFromPrefs ->
				val url = configFromPrefs.secureBaseUrl

				if (url == null) {
					apiService.configuration()
							.map { it.images }
							.doOnSuccess { configFromServer ->
								prefs.imagesConfiguration = configFromServer
							}
				} else {
					Single.just(configFromPrefs)
				}
			}
}