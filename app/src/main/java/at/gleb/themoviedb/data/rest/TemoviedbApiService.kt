package at.gleb.themoviedb.data.rest

import androidx.annotation.IntRange
import at.gleb.themoviedb.domain.entities.*
import at.gleb.themoviedb.domain.entities.media.MediaType
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TemoviedbApiService {
	@GET("/3/movie/popular")
	fun popularMovies(@Query("language") language: String? = null,
	                  @Query("page") @IntRange(from = 1, to = 1000) page: Int = 1,
	                  @Query("region") region: String? = null
	): Single<MoviesResponse>

	@GET("/3/configuration")
	fun configuration(): Single<ConfigurationResponse>


	@GET("/3/search/multi")
	fun multiSearch(
			@Query("query") query: String,
			@Query("language") language: String? = null,
			@Query("region") region: String? = null,
			@Query("page") @IntRange(from = 1, to = 1000) page: Int = 1

	): Single<SearchResponse>

	@GET("/3/person/{person_id}/tv_credits")
	fun personTvCredits(@Path("person_id") personId: Int, @Query("language")
	language: String? = null
	): Single<PersonCreditsResponse<MediaType.Tv>>

	@GET("/3/person/{person_id}/movie_credits")
	fun personMovieCredits(@Path("person_id") personId: Int, @Query("language")
	language: String? = null
	): Single<PersonCreditsResponse<MediaType.Movie>>

	@GET("/3/person/{person_id}")
	fun personDetails(@Path("person_id") personId: Int, @Query("language") language: String? = null): Single<PersonDetailsResponse>
}