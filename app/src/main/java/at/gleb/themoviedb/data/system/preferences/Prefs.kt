package at.gleb.themoviedb.data.system.preferences

import at.gleb.prefdeletages.LongPref
import at.gleb.prefdeletages.StringPref
import at.gleb.themoviedb.domain.entities.ImagesConfiguration

private const val STORAGE_TIME_MILLS = 259200000 //3 days
private const val separator = ","

class Prefs {
	var imagesConfiguration: ImagesConfiguration
		set(value) {
			value.baseUrl?.let {
				baseUrl = it
			}
			value.secureBaseUrl?.let {
				secureBaseUrl = it
				updateTime = System.currentTimeMillis() + STORAGE_TIME_MILLS
				posterSizes = value.posterSizes.joinToString(separator)
				profileSizes = value.profileSizes.joinToString(separator)
			}
		}
		get() = if (System.currentTimeMillis() > updateTime) {
			//todo: use Optional instead
			ImagesConfiguration(null, null)
		} else {
			ImagesConfiguration(baseUrl = baseUrl,
			                    secureBaseUrl = secureBaseUrl,
			                    posterSizes = posterSizes.split(separator),
			                    profileSizes = profileSizes.split(separator))
		}

	private var baseUrl by StringPref("")
	private var secureBaseUrl by StringPref("")
	private var updateTime by LongPref(0)
	private var posterSizes by StringPref("")
	private var profileSizes by StringPref("")
}