package at.gleb.themoviedb.data

enum class CastType(val num: Int) {
	MOVIES(0),
	TV(1)
}

fun Int?.toCastType(): CastType? = when (this) {
	0 -> CastType.MOVIES
	1 -> CastType.TV
	else -> null

}